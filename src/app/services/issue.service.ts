import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Issue } from 'app/models/issue.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IssueService {

  constructor(protected http: HttpClient) { }

  all(): Observable<Issue[]> {
    return this.http.get<Issue[]>('/api/v1/issues');
  }
}
