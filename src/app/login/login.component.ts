import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularTokenService } from 'angular-token';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;

  constructor(
    private router: Router,
    private tokenService: AngularTokenService
  ) {}

  ngOnInit() {}

  login() {
    console.log('Login realizado');
    this.tokenService
      .signIn({
        login: 'admin@example.com',
        password: '123456'
      })
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/dashboard']);
        },
        error => console.log(error)
      );
  }
}
